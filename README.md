# Projeto_Kratos
Projeto para medição de temperatura e umidade do ambiente usando esp32

Link para o projeto original: [original_code](https://capsistema.com.br/index.php/2020/02/02/servidor-da-web-esp32-dht11-dht22-temperatura-e-umidade-usando-o-arduino-ide/)

Adaptado por: Philippe Costa, João Victor Farias, Sergio Baez
